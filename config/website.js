module.exports = {
  _pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"
  _title: 'Daryll Cheng :: Software Engineer', // Navigation and Site Title
  _titleAlt: 'Daryll Cheng :: Software Engineer', // Title for JSONLD
  description: 'Behavior therapist turned engineer now combining computer science with a deep understanding of human psychology to create the most compelling and immersive web experiences',
  _url: 'https://www.daryll.io', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  logo: 'src/assets/android-chrome-512x512.png', // Used for SEO
  image: '/assets/daryllio.png',

  // JSONLD / Manifest
  favicon: 'src/assets/android-chrome-512x512.png', // Used for manifest favicon generation
  shortName: 'Daryll Cheng', // shortname for manifest. MUST be shorter than 12 characters
  author: 'Daryll', // Author for schemaORGJSONLD
  themeColor: '#3D63AE',
  backgroundColor: '#EBEDF2',

  twitter: '@kbdw248', // Twitter Username
};