import { faFacebook, faGithub, faGoogle, faLinkedin } from '@fortawesome/free-brands-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import personal from '../../data/personal.json'

const Footer = props => (
  <div className="section">
    <div id="footer" className="hero">
      <div className="overlay">
        <div className="wrap">
          <div className="footerSocial">
            <a href={personal.Github} target="_blank">
              <FontAwesomeIcon
                className="fab fa-github"
                icon={faGithub}
              />
            </a>
            <a href={personal.Linkedin} target="_blank">
              <FontAwesomeIcon
                className="fab fa-linkedin"
                icon={faLinkedin}
              />
            </a>
            <a href={personal.Facebook} target="_blank">
              <FontAwesomeIcon
                className="fab fa-facebook"
                icon={faFacebook}
              />
            </a>
            <a href={personal.GooglePhotos} target="_blank">
              <FontAwesomeIcon
                className="fab fa-google"
                icon={faGoogle}
              />
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Footer
