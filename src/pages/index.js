import "../styles/main.scss"

import React, { Component } from 'react'

import Footer from '../components/Footer/footer.jsx'
import Intro from '../components/Intro/intro.jsx'
import Nav from '../components/Nav/nav.jsx'
import Projects from '../components/Projects/projects.jsx'
import SEO from '../components/seo'
import projectData from '../data/projectData.json'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      projects: projectData,
    }
  }
  render() {
    return (
      <div className="site-container">
        <SEO keywords={[`daryll`, `portfolio`, `gatsby`]} />
        <Nav projects={this.state.projects} />
        <Intro />
        <Projects projects={this.state.projects} />
        <Footer />
      </div>
    )
  }
}

export default App
